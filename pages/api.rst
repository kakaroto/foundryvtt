.. _api:

Developer API Documentation
***************************

This section of the documentation is dedicated to the software APIs which enable system and module developers to create
amazing add-on software which augments and extends the base functionality of the FVTT platform.

..  toctree::
    :caption: API Contents
    :name: api-contents
    :maxdepth: 2

    api/entities
    api/canvas
    api/dice
    api/hooks
    api/ui
