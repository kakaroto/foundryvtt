..  _dialog:

Dialog Window
*************

The :class:`Dialog` class provides an extension of :class:`Application` which makes it easy to create modal
dialog prompts.

.. autoclass:: Dialog
    :members:

    .. autofunction:: Application#render
